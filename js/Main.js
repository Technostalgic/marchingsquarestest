///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

/** @type {HTMLCanvasElement} */
var canvas = null;

/** @type {CanvasRenderingContext2D} */
var ctx = null;

/** @type {Array<Array<Number>>} */
var tiles = null;

/** @type {Number} calculated at runtime */
var tileColumns = 0;

/** @type {Number} calculated at runtime */
var tileRows = 0;

// just some values used to calculate the tile values from the noise function
var noiseOffset = 0;
var noiseScale = 0.25;

// time keeping variables used for performance benchmarking, this algorithm is not optomized so
// don't expect best results, but I wanted it to run reasonably fast
var lastTime = 0;
var fps = 0;

// the threshold value used to determine the surface of the mesh generated from marching squares
var threshold = 0.5;

// whether or not interpolation is enabled
var interpolate = true;

// whether or not the vertex values of the squares are drawn
var drawTileValues = false;

// the html slider control that the user can use to modify the threshold value
var control_threshold = null;

// the html checkbox control that the user can use to modify the interpolate variable
var control_interpolate = null;

// the html control that the user can use to modify the drawTileVals variable
var control_drawTileVals = null;

// how many adjacecnt tiles next to the mouse the terrain modification effects
var mouseRadius = 7;

// the intensity of the terrain deformation
var mouseIntensity = 0.1;

// variables used to track the mouse state
var mousePos = Vec2.zero;
var touchMode = 1;
var leftClick = false;
var rightClick = false;

/**
 * generates a random value between 0 and 1 based on a seed
 * @param {Number} seed the seed to use to generate the random number
 * @returns 
 */
function SeededRandom(seed) {

	// idk some bit magic shit that I found online
	let t = seed += 0x6D2B79F5;
	t = Math.imul(t ^ t >>> 15, t | 1);
	t ^= t + Math.imul(t ^ t >>> 7, t | 61);
	return ((t ^ t >>> 14) >>> 0) / 4294967296;
}

/**
 * just some smooth noise algorithm I made up, it's not really relevant to marching squares so I
 * won't explain it too much, but it's similar to perlin noise just not as good
 * @param {Number} x the x-offset for the noise value
 * @param {Number} y the y-offset for the noise value
 */
function Noise(x, y){

	let rsize = 1000;

	let minX = Math.floor(x);
	let maxX = Math.ceil(x);
	
	let minY = Math.floor(y);
	let maxY = Math.ceil(y);

	let vecTL = Vec2.FromDirection(SeededRandom(minX * rsize + minY) * Math.PI * 2);
	let vecTR = Vec2.FromDirection(SeededRandom(maxX * rsize + minY) * Math.PI * 2);
	let vecBL = Vec2.FromDirection(SeededRandom(minX * rsize + maxY) * Math.PI * 2);
	let vecBR = Vec2.FromDirection(SeededRandom(maxX * rsize + maxY) * Math.PI * 2);

	let xDif = x - minX;
	let vecT = Vec2.Lerp(vecTL, vecTR, xDif);
	let vecB = Vec2.Lerp(vecBL, vecBR, xDif);

	let target = Vec2.Lerp(vecT, vecB, y - minY);

	return target.magnitude;
}

/**
 * initialization of the program and set up loop
 */
function Initialize(){

	// get a reference to the canvas in the html document and create a render context from it
	canvas = document.getElementsByTagName("canvas")[0];
	ctx = canvas.getContext("2d");

	// find the checkbox for interpolation and add functionality for it's interaction
	control_interpolate = document.getElementById("control_interp");
	control_interpolate.checked = interpolate;
	control_interpolate.addEventListener("change", function(e){
		interpolate = control_interpolate.checked;
	})

	// find the checkbox for drawing tile values and add functionality for it's interaction
	control_drawTileVals = document.getElementById("control_drawTiles");
	control_drawTileVals.checked = drawTileValues;
	control_drawTileVals.addEventListener("change", function(e){
		drawTileValues = control_drawTileVals.checked;
	})

	control_threshold = document.getElementById("control_threshold");
	control_threshold.value = threshold * 100;
	control_threshold.addEventListener("input", function(e){
		threshold = control_threshold.value / 100;
	})

	// offset the noise by a random value so it doesn't generate the same pattern every time
	noiseOffset = Math.random() * 99999;

	// add event listeners to keep track of the mouse state
	AddMouseEvents();

	// override right click context menu so you can use right click to modify terrain
	canvas.oncontextmenu = function(e){ return false; } ;

	// generate a noise tile grid of the given resolution
	BuildRandomTiles(32, 32);

	// start the loop for drawing and handling user input
	Step();
}

/** 
 * attach the callbacks to mouse events so that the mouse state can be tracked 
 */
function AddMouseEvents(){

	canvas.addEventListener("touchstart", function(e){
		touchMode *= -1;
		mousePos.x = e.touches[0].clientX - canvas.offsetLeft;
		mousePos.y = e.touches[0].clientY - canvas.offsetTop;
		leftClick = touchMode == 1;
		rightClick = touchMode != 1;
	});
	canvas.addEventListener("touchend", function(e){
		leftClick = false;
		rightClick = false;
	});
	canvas.addEventListener("touchmove", function(e){
		mousePos.x = e.touches[0].clientX - canvas.offsetLeft;
		mousePos.y = e.touches[0].clientY - canvas.offsetTop;
		leftClick = touchMode == 1;
		rightClick = touchMode != 1;
	});

	// add event listeners to keep track of the mouse state
	canvas.addEventListener("mousemove", function(e){
		mousePos.x = e.offsetX;
		mousePos.y = e.offsetY;
	});
	canvas.addEventListener("mousedown", function(e){
		e.preventDefault();
		leftClick = ((1 << 0) & e.buttons) > 0;
		rightClick = ((1 << 1) & e.buttons) > 0;
	});
	canvas.addEventListener("mouseup", function(e){
		e.preventDefault();
		leftClick = ((1 << 0) & e.buttons) > 0;
		rightClick = ((1 << 1) & e.buttons) > 0;
	});
}

/**
 * initialize a 2d tile array of the specified size where each tile has a randomized value
 * @param {Number} width how many tile columns there are
 * @param {Number} height how many tile rows there are
 */
function BuildRandomTiles(width, height){

	tileColumns = width;
	tileRows = height;

	// reset the tile array
	tiles = [];

	// iterate through each integer value in the area of the sepecified size
	for(let x = 0; x < width; x++){

		// ensure there is a nested array at the specified location
		if(tiles[x] == null){
			tiles[x] = [];
		}
		
		for (let y = 0; y < height; y++){

			// set the tile to a noise value based on it's grid position
			tiles[x][y] = Noise(noiseOffset + x * noiseScale, noiseOffset + y * noiseScale);
		}
	}
}

/**
 * call the draw method and set up another callback for it that continues indefinitely
 */
function Step(){

	if(leftClick){
		ModifyTerrain(1);
	}
	else if(rightClick){
		ModifyTerrain(-1);
	}

	Draw();
	requestAnimationFrame(function(){
		Step();
	});
}

/**
 * modifies the terrain around the current mouse position
 * @param {Number} intensity the max delta value to change the tile values by
 */
function ModifyTerrain(intensity){
	
	// calculate the tile size based off the canvas size and tile resolution
	let tilesize = new Vec2(canvas.width / (tileColumns - 1), canvas.height / (tileRows - 1));

	// get the tile that the mouse currently lies on
	let mouseTile = new Vec2(
		Math.floor(mousePos.x / tilesize.x), 
		Math.floor(mousePos.y / tilesize.y));

	// calculate the 2d indices to iterate through based on the mouse position and radius
	let minX = Math.max(mouseTile.x - mouseRadius, 0);
	let maxX = Math.min(mouseTile.x + mouseRadius, tileColumns - 1);
	let minY = Math.max(mouseTile.y - mouseRadius, 0);
	let maxY = Math.min(mouseTile.y + mouseRadius, tileRows - 1);

	// iterate through each of those indices which represent tile vertex indices
	for(let x = minX; x <= maxX; x++){
		for(let y = minY; y <= maxY; y++){

			// find the position difference between the mouse tile and the currently iterated tile
			let xDif = mouseTile.x - x;
			let yDif = mouseTile.y - y;

			// calculate euclidean distance from the differences calculated above
			let dist = Math.max(1 - Math.sqrt(xDif * xDif + yDif * yDif) / mouseRadius, 0);
			dist *= dist;

			// increment the tile's value based on the given intensity value and the distance
			tiles[x][y] = Math.min(Math.max(tiles[x][y] + dist * intensity * mouseIntensity, 0), 1);
		}
	}
}

/**
 * render the screen and all the marching squares
 */
function Draw(){

	// clear the canvas
	ctx.fillStyle = "#55F";
	ctx.fillRect(0, 0, canvas.width, canvas.height);

	// find the tilesize based off the canvas size
	let tilesize = new Vec2(canvas.width / (tileColumns - 1), canvas.height / (tileRows - 1));

	// iterate through each square
	for(let x = 0; x < tileColumns - 1; x++){
		for(let y = 0; y < tileRows - 1; y++){

			// calculate the vertex values of each corner of the square
			let valueTL = tiles[x][y];
			let valueTR = tiles[x+1][y];
			let valueBL = tiles[x][y+1];
			let valueBR = tiles[x+1][y+1];

			// find the position of the square based on it's top left vertex
			let tilePos = tilesize.Scaled(new Vec2(x, y));

			// draw the square, given the values we've just calculated
			DrawSquare(tilePos, tilesize, valueTL, valueTR, valueBL, valueBR);
		}
	}
	
	// draw the indivitual tile values if specified
	if(drawTileValues)
		DrawTileValues(tilesize)

	// calculate the frames per second for performance benchmarking
	let now = performance.now();
	fps = 1000 / (now - lastTime);
	lastTime = now;
}

/**
 * draws a square based on it's corner tile values, this is basically where the magic of the 
 * marching squares algorithm is
 * @param {Vec2} pos the position of the top left of the square
 * @param {Vec2} size the size of the square (width, height)
 * @param {Number} valueTL the value at the tile that represents the square's top left corner
 * @param {Number} valueTR the value at the tile that represents the square's top right corner
 * @param {Number} valueBL the value at the tile that represents the square's bottom left corner
 * @param {Number} valueBR value of the tile vertex that represents the square's bottom right corner
 */
function DrawSquare(pos, size, valueTL, valueTR, valueBL, valueBR){

	let tl = valueTL >= threshold;
	let tr = valueTR >= threshold;
	let bl = valueBL >= threshold;
	let br = valueBR >= threshold;

	// the value that gets interpolated, represents the midpoint weight between two corners, 0.5
	// will be directly in between those two corners, where 0.75 will be 3/4ths of the way, etc
	let val = 0.5;

	// apply the interpolation value for the top midpoint if interpolation is enabled
	if(interpolate){
		val = FindInterpolationDelta(valueTL, valueTR);
	}

	// find the approriate midpoint position between top left and top right
	let tmid = pos.clone;
	tmid.x += val * size.x;

	// apply the interpolation value for the left midpoint if interpolation is enabled
	if(interpolate){
		val = FindInterpolationDelta(valueTL, valueBL);
	}

	// find the approriate midpoint position between top left and bottom left
	let lmid = pos.clone;
	lmid.y += val * size.y;

	// apply the interpolation value for the bottom midpoint if interpolation is enabled
	if(interpolate){
		val = FindInterpolationDelta(valueBL, valueBR);
	}
	
	// find the approriate midpoint position between bottom left and bottom right
	let bmid = pos.Plus(new Vec2(0, size.y));
	bmid.x += val * size.x;

	// apply the interpolation value for the right midpoint if interpolation is enabled
	if(interpolate){
		val = FindInterpolationDelta(valueTR, valueBR);
	}
	
	// find the approriate midpoint position between top right and bottom right
	let rmid = pos.Plus(new Vec2(size.x, 0));
	rmid.y += val * size.y;

	// determine which of the 16 square configurations the current square is
	let configuration = (
		bl << 0 |
		br << 1 |
		tr << 2 |
		tl << 3 
	);

	// apply the white stroke and fill colors to the render context
	ctx.fillStyle = "#FFF8"; // white with partial transparency
	ctx.strokeStyle = "#FFF"; // opaque white
	ctx.lineWidth = 1;

	// draw the square based on it's configuration
	switch(configuration){

		case 0: // square is completely empty
			break;

		// 1 point:
		case 1: // bottom left corner
			ctx.beginPath();
			ctx.moveTo(lmid.x, lmid.y);
			ctx.lineTo(bmid.x, bmid.y);
			ctx.stroke();
			ctx.lineTo(pos.x, pos.y + size.y);
			ctx.fill();
			break;
		case 2: // bottom right corner
			ctx.beginPath();
			ctx.moveTo(bmid.x, bmid.y);
			ctx.lineTo(rmid.x, rmid.y);
			ctx.stroke();
			ctx.lineTo(pos.x + size.x, pos.y + size.y);
			ctx.fill();
			break;
		case 4: // top right corner
			ctx.beginPath();
			ctx.moveTo(tmid.x, tmid.y);
			ctx.lineTo(rmid.x, rmid.y);
			ctx.stroke();
			ctx.lineTo(pos.x + size.x, pos.y);
			ctx.fill();
			break;
		case 8: // top left corner
			ctx.beginPath();
			ctx.moveTo(lmid.x, lmid.y);
			ctx.lineTo(tmid.x, tmid.y);
			ctx.stroke();
			ctx.lineTo(pos.x, pos.y);
			ctx.fill();
			break;

		// 2 point:
		case 3: // bottom half
			ctx.beginPath();
			ctx.moveTo(lmid.x, lmid.y);
			ctx.lineTo(rmid.x, rmid.y);
			ctx.stroke();
			ctx.lineTo(pos.x + size.x, pos.y + size.y);
			ctx.lineTo(pos.x, pos.y + size.y);
			ctx.fill();
			break;
		case 12: // top half
			ctx.beginPath();
			ctx.moveTo(lmid.x, lmid.y);
			ctx.lineTo(rmid.x, rmid.y);
			ctx.stroke();
			ctx.lineTo(pos.x + size.x, pos.y);
			ctx.lineTo(pos.x, pos.y);
			ctx.fill();
			break;
		case 6: // right half
			ctx.beginPath();
			ctx.moveTo(tmid.x, tmid.y);
			ctx.lineTo(bmid.x, bmid.y);
			ctx.stroke();
			ctx.lineTo(pos.x + size.x, pos.y + size.y);
			ctx.lineTo(pos.x + size.x, pos.y);
			ctx.fill();
			break;
		case 9: // left half
			ctx.beginPath();
			ctx.moveTo(tmid.x, tmid.y);
			ctx.lineTo(bmid.x, bmid.y);
			ctx.stroke();
			ctx.lineTo(pos.x, pos.y + size.y);
			ctx.lineTo(pos.x, pos.y);
			ctx.fill();
			break;
		case 5: // top left and bottom right corners
			ctx.beginPath();
			ctx.moveTo(lmid.x, lmid.y);
			ctx.lineTo(tmid.x, tmid.y);
			ctx.stroke();
			ctx.beginPath();
			ctx.moveTo(bmid.x, bmid.y);
			ctx.lineTo(rmid.x, rmid.y);
			ctx.stroke();
			ctx.beginPath();
			ctx.moveTo(lmid.x, lmid.y);
			ctx.lineTo(tmid.x, tmid.y);
			ctx.lineTo(pos.x + size.x, pos.y)
			ctx.lineTo(rmid.x, rmid.y);
			ctx.lineTo(bmid.x, bmid.y);
			ctx.lineTo(pos.x, pos.y + size.y)
			ctx.fill();
			break;
		case 10: // top right and bottom left corners
			ctx.beginPath();
			ctx.moveTo(lmid.x, lmid.y);
			ctx.lineTo(bmid.x, bmid.y);
			ctx.stroke();
			ctx.beginPath();
			ctx.moveTo(tmid.x, tmid.y);
			ctx.lineTo(rmid.x, rmid.y);
			ctx.stroke();
			ctx.beginPath();
			ctx.moveTo(lmid.x, lmid.y);
			ctx.lineTo(bmid.x, bmid.y);
			ctx.lineTo(pos.x + size.x, pos.y + size.y);
			ctx.lineTo(rmid.x, rmid.y);
			ctx.lineTo(tmid.x, tmid.y);
			ctx.lineTo(pos.x, pos.y);
			ctx.fill();
			break;

		// 3 point:
		case 7: // all corners except top left
			ctx.beginPath();
			ctx.moveTo(lmid.x, lmid.y);
			ctx.lineTo(tmid.x, tmid.y);
			ctx.stroke();
			ctx.lineTo(pos.x + size.x, pos.y);
			ctx.lineTo(pos.x + size.x, pos.y + size.y);
			ctx.lineTo(pos.x, pos.y + size.y);
			ctx.fill();
			break;
		case 11: // all corners except top right
			ctx.beginPath();
			ctx.moveTo(tmid.x, tmid.y);
			ctx.lineTo(rmid.x, rmid.y);
			ctx.stroke();
			ctx.lineTo(pos.x + size.x, pos.y + size.y);
			ctx.lineTo(pos.x, pos.y + size.y);
			ctx.lineTo(pos.x, pos.y);
			ctx.fill();
			break;
		case 13: // all corners except bottom right
			ctx.beginPath();
			ctx.moveTo(bmid.x, bmid.y);
			ctx.lineTo(rmid.x, rmid.y);
			ctx.stroke();
			ctx.lineTo(pos.x + size.x, pos.y);
			ctx.lineTo(pos.x, pos.y);
			ctx.lineTo(pos.x, pos.y + size.y);
			ctx.fill();
			break;
		case 14: // all corners except bottom left
			ctx.beginPath();
			ctx.moveTo(lmid.x, lmid.y);
			ctx.lineTo(bmid.x, bmid.y);
			ctx.stroke();
			ctx.lineTo(pos.x + size.x, pos.y + size.y);
			ctx.lineTo(pos.x + size.x, pos.y);
			ctx.lineTo(pos.x, pos.y);
			ctx.fill();
			break;
		
		// 4 point:
		case 15: // square is completely full
			ctx.fillRect(pos.x, pos.y, size.x, size.y);
	}
}

/**
 * @type {Number} returns a value from 0 to 1 of how interpolated the value from tile 1 to tile 2, 
 * 	the midpoint should be. 0 represents no interpolation, so the midpoint value would just be the 
 * 	vertex position of tile 1, and 1 represents full interpolation so the midpoint value would just
 * 	be the vertex position of tile 2
 * @param {Number} tile1 the value of the 1st tile vertex to interpolate from
 * @param {Number} tile2 the value of the 2nd tile vertex to interpolat to
 */
function FindInterpolationDelta(val1, val2){
	
	// just a really low value to account for floating point errors
	let epsilon = 0.001;
	
	// if the vertex value at tile 1 is equal to the threshold, no interpolation
	if(Math.abs(threshold - val1) < epsilon)
		return 0;
	
	// if the vertex value at tile 2 is equal to the threshold, full interpolation
	if(Math.abs(threshold - val2) < epsilon)
		return 1;

	// if the values are the same, it doesn't matter, so just return 0
	if(Math.abs(val1 - val2) < epsilon)
		return 0;

	// the delta interpolation value is equal to the difference between the threshold from first 
	// value over the difference of value 2 from value 1
	return (threshold - val1) / (val2 - val1);
}

/**
 * draws each of the tile vertex values (each of every square's corners) on the screen
 * @param {Vec2} tilesize the size of each individual square
 */
function DrawTileValues(tilesize){

	let tileRenderSize = tilesize.Scaled(0.4);
	
	// iterate through each tile vertex
	for(let x = tiles.length - 1; x >= 0; x--){
		for(let y = tiles[x].length - 1; y >= 0; y--){

			// find the position of the current tile vertex
			let tilePos = tilesize.Scaled(new Vec2(x, y)).Minus(tileRenderSize.Scaled(0.5));
			let value = tiles[x][y];

			// set the fill color and draw a rext at the calculated vertex position
			ctx.fillStyle = "rgb(" + value * 255 + "," + value * 255 + "," + value * 255 + ")";
			ctx.fillRect(tilePos.x, tilePos.y, tileRenderSize.x , tileRenderSize.y);
		}
	}

}