///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

/** enumerator for different 4-way directions */
var Direction = {
	up: 1,
	right: 2,
	left: 3,
	down: 4,

	/**
	 * @type {Direction} returns the direction 90 degrees clockwise to the specified direction
	 * @param {Direction} direction 
	 */
	RotateCW: function(direction){

		switch(direction){
			case Direction.up: return Direction.right;
			case Direction.right: return Direction.down;
			case Direction.left: return Direction.up;
			case Direction.down: return Direction.left;
		}
	},

	/**
	 * @type {Direction} returns the direction 90 degrees counter clockwise to the specified direction
	 * @param {Direction} direction 
	 */
	RotateCCW: function(direction){

		switch(direction){
			case Direction.up: return Direction.left;
			case Direction.right: return Direction.up;
			case Direction.left: return Direction.down;
			case Direction.down: return Direction.right;
		}
	},

	/**
	 * @type {Boolean} returns true if the specified direction is horizontal
	 * @param {Direction} direction 
	 */
	IsHorizontal: function(direction){
		return direction == 2 || direction == 3;
	},

	/** 
	 * @type {Direction} returns the opposite of the provided direction
	 * @param {Direction} direction the direction to get the opposite of 
	 */
	Opposite: function(direction){
		return 5 - direction;
	},

	/**
	 * @type {Number} returns the angle the direction is pointing in, in radians
	 * @param {Direction} direction 
	 */
	GetAngle: function(direction){
		switch(direction){
			case Direction.right: return 0;
			case Direction.left: return Math.PI;
			case Direction.up: return Math.PI / 2 * 3;
			case Direction.down: return Math.PI / 2;
		}
	},

	/**
	 * @type {Direction} returns a direction from a specified vector
	 * @param {Vec2} vector the direction vector
	 */
	FromVector: function(vector){
		
		let ang = vector.direction;
		ang /= Math.PI / 2;
		ang = Math.round(ang);
		ang %= 4;
		if(ang < 0){
			ang += 4;
		}

		switch(ang){
			case 0: return Direction.right;
			case 1: return Direction.down;
			case 2: return Direction.left;
			case 3: return Direction.up;
		}
	},

	/**
	 * @type {Vec2} returns a vector that the direction represents
	 * @param {Direction} direction 
	 */
	ToVector: function(direction){

		switch(direction){
			case Direction.up: return new Vec2(0, -1);
			case Direction.left: return new Vec2(-1, 0);
			case Direction.right: return new Vec2(1, 0);
			case Direction.down: return new Vec2(0, 1);
		}
	}
}

function AngleDifference(source, target)
{
	let a = target - source;
	a = ActualMod((a + Math.PI), (Math.PI * 2)) - Math.PI;
	return a;
}

function AngleDifferenceFactor(source, target){

	let dif = Math.abs(AngleDifference(source, target));
	return 2 - (dif / Math.PI) - 1;
}

function ActualMod(num, mod){
	 let a = num % mod;
	 if(a < 0)
		a += mod;
	return a;
}

/** a data structure that represents a 2 dimensional vector */
class Vec2 {

	/** initializes the vector with specified x and y components */
	constructor(x = 0, y = x) {

		this.x = x;
		this.y = y;
	}

	/** @type {Vec2} converts from anonymous object with x,y fields into vec2 */
	static FromObjectData(data){
		return new Vec2(data.x, data.y);
	}
	
	/** @type {Vec2} converts from a 2-length array into a vec2 */
	static FromArrayData(data){
		return new Vec2(data[0], data[1]);
	}

	/** @type {Vec2} returns a vector from the given angle and magnitude */
	static FromDirection(dir, magnitude = 1){
		return (new Vec2(Math.cos(dir), Math.sin(dir)).Times(magnitude));
	}
	
	/** @type {Vec2} */
	static RandomDirection(magnitude = 1){
		return Vec2.FromDirection(Math.random() * Math.PI * 2, magnitude);
	}
	
	/**
	 * @type {Vec2} returns a new vector that is linearly interpolated from two specified vectors
	 * @param {Number} a the vector to interpolate from
	 * @param {Number} b the vector to interpolate to
	 * @param {Number} delta the amount of progress that has been made from a to b, from 0 to 1
	 */
	static Lerp(a, b, delta = 0.5){
		let dif = b.Minus(a);
		return a.Plus(dif.Times(delta));
	}

	/** @type {Vec2} returns a vector that points left */
	static get Left(){ return new Vec2(-1, 0); }
	/** @type {Vec2} returns a vector that points Right */
	static get Right(){ return new Vec2(1, 0); }
	/** @type {Vec2} returns a vector that points Up */
	static get Up(){ return new Vec2(0, -1); }
	/** @type {Vec2} returns a vector that points Down */
	static get Down(){ return new Vec2(0, 1); }

	/** translates the vector by the specified vector offset */
	Translate(vec){
		this.x += vec.x;
		this.y += vec.y;
	}
	/** makes the vector's magnitude 1 */
	Normalize(){
		this.x /= this.magnitude;
		this.y /= this.magnitude;
	}

	/**
	 * @type {number} returns the dot product of this and another vector
	 * @param {Vec2} vec 
	 */
	Dot(vec){ return this.x * vec.x + this.y * vec.y; }
	/**@type {Vec2} adds the vector to another and returns the result */
	Plus(vec) {
		return new Vec2(this.x + vec.x, this.y + vec.y);
	}
	/**@type {Vec2} subtracts another vector from itself and returns the result */
	Minus(vec) {
		return this.Plus(vec.negated);
	}
	/**@type {Vec2} returns this vector multiplied by a numeric value */
	Times(/**@type {Number}*/ factor) {
		return new Vec2(this.x * factor, this.y * factor);
	}
	/**
	 * @type {Vec2} scales the vector by the specified factor and returns the result 
	 * @param {Vec2|Number} factor
	*/
	Scaled(factor) {
		if (factor instanceof Vec2)
			return new Vec2(this.x * factor.x, this.y * factor.y);
		return new Vec2(this.x * factor, this.y * factor);
	}
	/**
	 * @type {Vec2} rotates the vector around 0,0 by the specified angle and returns the result 
	 * @param {Number} angle in radians
	 */
	Rotated(angle){

		// store the angle and magnitue of the vector
		let ang = this.direction;
		let mag = this.magnitude;

		// offset the angle by the specified amount
		ang += angle;

		// calculate return the result
		return new Vec2(Math.cos(ang) * mag, Math.sin(ang) * mag);
	}

	/**@type {Vec2} rounds the vector's x and y to the nearest whole numbers */
	get rounded() {
		return new Vec2(Math.round(this.x), Math.round(this.y));
	}
	/**@type {Vec2} drops the vector's decimal parts of the x and y components and returns the result*/
	get floored(){
		return new Vec2(Math.floor(this.x), Math.floor(this.y));
	}

	/**@type {Vec2} returns the vector fliped along both axes */
	get negated() {
		return this.Scaled(-1);
	}
	/**@type {Vec2} returns 1 divided by this */
	get reciporocal() {
		return new Vec2(1 / this.x, 1 / this.y);
	}
	/**@type {Vec2} returns a vector with the same XY ratio but it's magnitude scaled to 1 */
	get normalized() {
		return this.Scaled(1 / this.magnitude);
	}

	/**@type {Number} returns the direction in radians that the vector is pointing in */
	get direction() {
		return Math.atan2(this.y, this.x);
	}
	/**@type {Number} returns the distance of the vector from the origin point <0,0> */
	get magnitude() {
		return Math.sqrt(this.x * this.x + this.y * this.y);
	}
	/**@type {Vec2} returns the vector rounded to the nearst whole number*/
	get rounded() {
		return new Vec2(Math.round(this.x), Math.round(this.y));
	}
	/**@type {Vec2} returns a new instance of the vector with the same values */
	get clone() {
		return new Vec2(this.x, this.y);
	}

	toString() {
		return "<" + this.x.toString() + "," + this.y.toString() + ">";
	}

	/** @returns {Array<Number>} */
	ToJSONArray(){
		return [this.x, this.y];
	}

	/**
	 * returns true if the vectors are aprocimately equal
	 * @param {Vec2} vecA 
	 * @param {Vec2} vecB 
	 * @param {Number} lenience the threshold that must be passed for comparison 
	 */
	static Approximately(vecA, vecB, lenience = 0.001){

		return (Math.abs(vecA.x - vecB.x) <= lenience && Math.abs(vecA.y - vecB.y) <= lenience);
	}

	static get zero() {
		return new Vec2(0, 0);
	}

	static get one() {
		return new Vec2(1, 1);
	}
}

/** a data structure used to represent a visual color */
class Color {

	constructor(/**@type {Number}*/r = 0, /**@type {Number}*/g = 0, /**@type {Number}*/b = 0, /**@type {Number}*/a = 1) {

		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	/** @type {Color} */
	static get Transparent(){ return new Color(0, 0, 0, 0); }
	/** @type {Color} */
	static get TranslucentBlack(){ return new Color(0, 0, 0, 0.5); }
	/** @type {Color} */
	static get White(){ return new Color(255, 255, 255); }
	/** @type {Color} */
	static get Black(){ return new Color(0, 0, 0); }
	/** @type {Color} */
	static get Grey(){ return new Color(128, 128, 128); }
	/** @type {Color} */
	static get Red(){ return new Color(255, 0, 0); }
	/** @type {Color} */
	static get Green(){ return new Color(0, 255, 0); }
	/** @type {Color} */
	static get Blue(){ return new Color(0, 0, 255); }

	/** returns a string representing the color in RGB format that can be used to format a canvasRenderingContext2d fill or line style */
	ToRGB() {

		return "rgb(" +
			Math.round(this.r).toString() + "," +
			Math.round(this.g).toString() + "," +
			Math.round(this.b).toString() + ")";
	}
	/** returns a string representing the color in RGBA format that can be used to format a canvasRenderingContext2d fill or line style */
	ToRGBA() {

		return "rgba(" +
			Math.round(this.r).toString() + "," +
			Math.round(this.g).toString() + "," +
			Math.round(this.b).toString() + "," +
			this.a.toString() + ")";
	}
}