# Marching Squares JS

A simple implementation of the marching squares algorithm in html5

In this example we generate a 2d array of floating point values and populate them based from a smooth noise algorithm. The Marching squares algorithm is used to extrapolate terrain-like contours from the 2d array based on the value at each index. The image generated is a result of the calculation of the contours where the white areas are parts of the terrain that are "full".

There are also some tweakable paramaters:

* interpolation - whether or not linear interpolation is enabled for the contour generation, if disabled each tile value is treated as a boolean value, whether or not it passes the threshold, whereas if enabled, the value will effect the contour even if the threshold is not met  
* draw tile values - whether or not the values of each tile in the array is drawn. A value of 0 will be depicted as a black square, and a value of 1 will be depicted as a white square, with varying levels of gray in between  
* tile threshold - the threshold value at which once a tile reaches this value, it will be considered "full"  

# [View Live Demo Here](https://codepen.io/TechnostalgicDev/pen/JjWWKjB)